function __ps1_git_location {
    GIT_BRANCH=$(git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')

    if [ -n "$GIT_BRANCH" ]
    then
        if [ "$GIT_BRANCH" = "(no branch)" ]
        then
           LOCATION=$(git describe)
         else
           LOCATION=$GIT_BRANCH
         fi

        echo " ($LOCATION) "
    else
        echo " "
   fi
}

function __ps1_filesize
{
    expr match "$(ls -lah)" 'total \([0-9a-Z]*\)'
}

export PS1="[\[\033[1;34m\]\u@\h\[\e[0m\] \[\033[1;31m\]\w\$(__ps1_git_location)\[\e[0m\]\[\033[32m\]\$(__ps1_filesize)\[\e[0m\]]\$ "

